#!/bin/bash
echo version 2023-0219-1
HOME=~


# LibreWolf download page: https://librewolf.net/installation/linux/
LIBREWOLF_VERSION="100.0.2-1"
LIBREWOLF_SHA256=b43ccac2acac4d0f6b54a212f2a4364dbd5bc35cf7ad2e460da71509dbf2b723
LIBREWOLF_VERSION="103.0.1-1"
LIBREWOLF_SHA256=a26061c4cb8fcb6d0eb44dcda58109331094b5e8e007712af2d45f5e5df5dda6
LIBREWOLF_VERSION="105.0.3-1"
LIBREWOLF_SHA256=c52ed5b557a6fb413ab36fb1cd1d13dc90b5ae10e4938a389f39c887ab8cc860
LIBREWOLF_VERSION="106.0.1-1"
LIBREWOLF_SHA256=2c3cabbf67f0cf88d74d0ebb76f6f8671e78dc1caac76cba8c67708433285613
LIBREWOLF_VERSION="107.0-1"
LIBREWOLF_SHA256=d703e9b5837cd85b13119c1760568292e364b061dddf76dccbde4c3089aa90a1
LIBREWOLF_VERSION="108.0.1-1"
LIBREWOLF_SHA256=84c007a8d5b0c6291c13e6bd5ef6d9b82f3c695ad0e3ef4432997b760aac1371
LIBREWOLF_VERSION="110.0-2"
LIBREWOLF_SHA256=ea4071a3a9d3e5808b88e09287e286948a06db5d3a06976fb10c4606c3b05f46
# timezone
TIMEZONE="Europe/Amsterdam"


# ----- do not edit below this line -----

# erste Aktion, damit die Passwortabfrage vom Benutzer gleich am Anfang durchgefuhrt werden kann und das Skript ohne Benutzerinteraktion durchlaufen kann
#  -> first action, so that the password request can be performed by the user right at the beginning and the script can run through without user interaction
# first action because the passwort input must be done at the beginning so the script can be running without user interaction
#  -> erste Aktion, da die Passworteingabe zu Beginn erfolgen muss, damit das Skript ohne Benutzerinteraktion ausgeführt werden kann
sudo true


function establish_base {
set -e
    echo "deleting directories in ${HOME}"
    cd "${HOME}"
    rm -rf ./Desktop ./Documents ./Music ./Pictures ./Public ./Templates ./Videos ./.mozilla ./.cache .config/gtk-3.0/* ./software

    echo "creating directories"
    mkdir -p "${HOME}/bookmarkmanagement/"
    mkdir -p "${HOME}/software/"
    mkdir -p "${HOME}/tmp/"
    mkdir -p "${HOME}/uploads/"

    echo "move ${0} to ${HOME}/software/"
    mv -t "${HOME}/software/" "${HOME}/Downloads/${0}"

    mkdir -p "${HOME}/tmp/logs-from-establishing-system/"
    readonly PATH_LOGS="$(echo $(cd "${HOME}/tmp/logs-from-establishing-system/" && pwd))"  # https://stackoverflow.com/questions/3915040
}


function establish_librewolf {
    install_librewolf
    setup_librewolf_profile
    create_file_userjs
# disabled because TST has not full permissions for bookmark management
#    create_file_userchromecss

    echo -e "\e[32m LibreWolf fully established \e[0m"
}


function install_stuff_via_apt {
    echo function install_stuff_via_apt
set -e
    readonly FFP_LOG_APT="${PATH_LOGS}/apt-stuff.stdall"

    echo "distribution specific actions"
    #TBD: move this to a new function for distribution detection
    OS_NAME=$(. /etc/os-release && echo $ID)
    if [ "${OS_NAME}" = "ubuntu" ]; then
        echo "detected ubuntu"
        sudo add-apt-repository universe 1>>"${FFP_LOG_APT}" 2>&1
    fi

    if [ "${OS_NAME}" != "linuxmint" ]; then
        while
            # on linuxmint 20.2 this ends with an error
            echo sudo apt update
            sudo apt update 1>>"${FFP_LOG_APT}" 2>&1
            [[ $? -ne 0 ]]
        do
            sleep 10
            # How to get rid of this while-loop in MX Linux:
            # E: Could not get lock /var/lib/apt/lists/lock. It is held by process 3981 (packagekitd)
            # sudo killall packagekitd  # MX Linux
        done
    fi


    echo sudo apt install -y feh tree meld gkrellm
    sudo apt install -y feh tree meld 1>>"${FFP_LOG_APT}" 2>&1


    echo "clean up apt stuff"
    sudo apt remove -y firefox 1>>"${FFP_LOG_APT}" 2>&1
    sudo apt autoremove -y 1>>"${FFP_LOG_APT}" 2>&1 # Remove automatically all unused packages
    sudo apt purge 1>>"${FFP_LOG_APT}" 2>&1 # Remove packages and their configuration files
    sudo apt clean 1>>"${FFP_LOG_APT}" 2>&1 # Erase downloaded archive files
    sudo apt autoclean 1>>"${FFP_LOG_APT}" 2>&1 # Erase old downloaded archive files
}


function config_desktop_settings {
set -e
    echo "set keyboard layout"
    setxkbmap -model pc105 -layout us -variant altgr-intl 


    echo "set timezone"
    if [ -d /run/systemd/system ]; then
        echo "systemd is running"
        sudo timedatectl set-timezone ${TIMEZONE}
    else
        sudo rm -rf /etc/localtime
        sudo ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
    fi


    echo "killing firefox"
    killall -s SIGKILL firefox 2>/dev/null || true
    killall -s SIGKILL firefox-bin 2>/dev/null ||:


    #TBD: move this to a new function for distribution detection
    DISTRIBUTION=$(. /etc/lsb-release && echo $DISTRIB_ID)
    if [ "${DISTRIBUTION}" = "MX" ]; then
        echo "kill conky"
        killall conky 2>/dev/null ||:

        echo "kill kdeconnectd"
        killall kdeconnectd 2>/dev/null ||:

        echo "create dark theme"
        create_file_kdeglobals
        create_file_gtksettingsini

        echo "set taskbar"
        move_taskbar_position
    fi
}


function move_taskbar_position {
set -e
    #.config/plasma-org.kde.plasma.desktop-appletsrc
    #>>bottom
        #formfactor=2
        #location=4
    #>>left
        #formfactor=3
        #location=5
    #>>top
        #formfactor=2
        #location=3
    #>>right
        #formfactor=3
        #location=6

    # move taskbar to right side
    sed -i "/formfactor=/c\formfactor=3" "${HOME}/.config/plasma-org.kde.plasma.desktop-appletsrc"
    sed -i "/location=/c\location=6" "${HOME}/.config/plasma-org.kde.plasma.desktop-appletsrc"

    # remove wallpaper
    sed -i "/Image=/c\Image=" "${HOME}/.config/plasma-org.kde.plasma.desktop-appletsrc"

    comment () {
        [Containments][29]
        activityId=d3d86ed4-6e67-47ce-b681-5e7f4e9aae0f
        formfactor=0
        immutability=1
        lastScreen=0
        location=0
        plugin=org.kde.plasma.folder
        wallpaperplugin=org.kde.color

        [Containments][29][Wallpaper][org.kde.color][General]
        Color=0,0,0

        [Containments][29][Wallpaper][org.kde.image][General]
        Image=file:///usr/share/backgrounds/Honesty.jpg
    }

    # add this if taskbar is on left/right side
    cat > "${HOME}/.config/plasmashellrc" <<- !HEREDOC
[PlasmaViews][Panel 2][Vertical2160]
thickness=44
!HEREDOC
#    printf "[PlasmaViews][Panel 2][Defaults]\nthickness=64\n" > "${HOME}/.config/plasmashellrc"

    plasmashell --replace 1>${PATH_LOGS}/plasmashell.stdall 2>&1 &
    sleep 3
    rm -rf ${HOME}/Desktop
}


function create_file_gtksettingsini {
    local DIR="${HOME}/.config/gtk-3.0/"
    local FILE="settings.ini"
    local FFP="$(echo $(cd "${DIR}" && pwd)/${FILE})"  # https://stackoverflow.com/questions/3915040

    echo "creating ${FFP}"
    cat > "${FFP}" << 'HEREDOC'
[Settings]
gtk-application-prefer-dark-theme=true
gtk-icon-theme-name=breeze-dark
gtk-modules=colorreload-gtk-module
HEREDOC
}


function create_file_kdeglobals {
    local DIR=$(echo $(cd ${HOME}/.config/ && pwd))  # https://stackoverflow.com/questions/3915040
    local FFP="${DIR}/kdeglobals"

    echo "creating ${FFP}"
    cat > "${FFP}" << 'HEREDOC'
[$Version]
update_info=filepicker.upd:filepicker-remove-old-previews-entry,fonts_global.upd:Fonts_Global,fonts_global_toolbar.upd:Fonts_Global_Toolbar,icons_remove_effects.upd:IconsRemoveEffects,kwin.upd:animation-speed,style_widgetstyle_default_breeze.upd:StyleWidgetStyleDefaultBreeze

[ColorEffects:Disabled]
ChangeSelectionColor=
Color=56,56,56
ColorAmount=0
ColorEffect=0
ContrastAmount=0.65
ContrastEffect=1
Enable=
IntensityAmount=0.1
IntensityEffect=2

[ColorEffects:Inactive]
ChangeSelectionColor=true
Color=112,111,110
ColorAmount=0.025
ColorEffect=2
ContrastAmount=0.1
ContrastEffect=2
Enable=false
IntensityAmount=0
IntensityEffect=0

[Colors:Button]
BackgroundAlternate=189,195,199
BackgroundNormal=77,77,77
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[Colors:Complementary]
BackgroundAlternate=41,44,48
BackgroundNormal=35,38,41
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[Colors:Header]
BackgroundAlternate=189,195,199
BackgroundNormal=49,54,59
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[Colors:Selection]
BackgroundAlternate=29,153,243
BackgroundNormal=52,73,94
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=252,252,252
ForegroundInactive=252,252,252
ForegroundLink=253,188,75
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=239,240,241
ForegroundPositive=39,174,96
ForegroundVisited=189,195,199

[Colors:Tooltip]
BackgroundAlternate=77,77,77
BackgroundNormal=29,153,243
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[Colors:View]
BackgroundAlternate=41,44,48
BackgroundNormal=35,38,41
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[Colors:Window]
BackgroundAlternate=189,195,199
BackgroundNormal=49,54,59
DecorationFocus=29,153,243
DecorationHover=77,77,77
ForegroundActive=61,174,233
ForegroundInactive=127,140,141
ForegroundLink=41,128,185
ForegroundNegative=218,68,83
ForegroundNeutral=246,116,0
ForegroundNormal=252,252,252
ForegroundPositive=39,174,96
ForegroundVisited=127,140,141

[General]
BrowserApplication=firefox.desktop
ColorScheme=BreezeHighContrast
Name=MX-Comfort-Breeze
XftAntialias=true
XftHintStyle=hintslight
XftSubPixel=rgb
shadeSortColumn=true
widgetStyle=Breeze

[Icons]
Theme=Papirus-mxblue-darkpanes

[KDE]
ColorScheme=MXcomfortbreeze
LookAndFeelPackage=org.mxlinux.mx.desktop
contrast=4
widgetStyle=Breeze

[WM]
activeBackground=255,55,0
activeBlend=29,153,243
activeForeground=252,252,252
inactiveBackground=49,54,59
inactiveBlend=26,188,156
inactiveForeground=252,252,252
HEREDOC
}


function install_librewolf {
set -e
    echo "installing LibreWolf"
    cd "${HOME}/tmp/"
    TARGETDIR="${HOME}/software/librewolf-${LIBREWOLF_VERSION}"
    FILE="LibreWolf.${LIBREWOLF_VERSION}.AppImage"

    echo "downloading LibreWolf"
    wget -O "${FILE}" "https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/${LIBREWOLF_VERSION}/LibreWolf.x86_64.AppImage"

    echo "checking checksum"
    printf "${LIBREWOLF_SHA256} ${FILE}" | sha256sum -c

    echo "setting exec bit"
    chmod +x "${FILE}"

    echo "moving AppImage to target directory"
    mkdir "${TARGETDIR}"
    mv "${FILE}" "${TARGETDIR}"

    echo "creating symlink"
    sudo rm -rf /usr/bin/librewolf /usr/bin/lwolf /usr/bin/wolf
    sudo ln -s "${TARGETDIR}/${FILE}" /usr/bin/librewolf
    sudo ln -s "${TARGETDIR}/${FILE}" /usr/bin/lwolf
    sudo ln -s "${TARGETDIR}/${FILE}" /usr/bin/wolf

>"${HOME}/.local/share/applications/librewolf.desktop" \
echo '[Desktop Entry]
Name=LibreWolf Web Browser
Keywords=browser;lwolf;wolf
Exec=librewolf %u
Terminal=false
Type=Application
Icon=librewolf
Categories=Network;WebBrowser;Internet
MimeType=text/html;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
'

    echo "LibreWolf installed"
}


function setup_librewolf_profile {
set -e
    if [ ! -f /usr/bin/librewolf ]; then
        echo "librewolf not installed"
        return 1
    fi

    echo "killall librewolf if running"
    killall -s SIGKILL librewolf 2>/dev/null ||:

    echo "remove directory"
    rm -rf "${HOME}/.librewolf/"

    echo "start LibreWolf"
    librewolf &
    PID=$!
    echo "PID: ${PID}"
    sleep 1
    echo "kill LibreWolf"
    kill ${PID}
}


function create_file_userjs {
    local PROFILE_DIR=$(echo $(cd ${HOME}/.librewolf/*.default/ && pwd))  # https://stackoverflow.com/questions/3915040
    local FFP="${PROFILE_DIR}/user.js"

    echo "creating ${FFP}"
    cat > "${FFP}" <<- 'HEREDOC'
user_pref("browser.backspace_action", 0);
user_pref("browser.bookmarks.autoExportHTML", true);
user_pref("browser.chrome.site_icons", false);
user_pref("browser.download.useDownloadDir", true);
user_pref("browser.tabs.allowTabDetach", false);
user_pref("browser.tabs.inTitlebar", 1);
user_pref("browser.tabs.firefox-view", false);
user_pref("browser.tabs.tabmanager.enabled", false);

user_pref("devtools.command-button-screenshot.enabled", true);
//user_pref("devtools.everOpened", true);
user_pref("devtools.inspector.activeSidebar", "ruleview");
user_pref("devtools.inspector.selectedSidebar", "ruleview");
user_pref("devtools.inspector.three-pane-enabled", false);
user_pref("devtools.toolbox.footer.height", 600);
//user_pref("devtools.toolsidebar-height.inspector", 350);
user_pref("devtools.toolsidebar-width.inspector", 600);
//user_pref("devtools.toolsidebar-width.inspector.splitsidebar", 350);

//user_pref("dom.forms.autocomplete.formautofill", true);

user_pref("findbar.highlightAll", true);

user_pref("intl.accept_languages", "en-US, en");
user_pref("javascript.use_us_english_locale", true);

user_pref("layout.spellcheckDefault", 0);

user_pref("media.navigator.enabled", false);
user_pref("media.peerconnection.enabled", false);
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);

user_pref("pdfjs.disabled", true);

user_pref("privacy.resistFingerprinting.letterboxing", false);

user_pref("reader.parse-on-load.enabled", false);

user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
HEREDOC
}


function create_file_userchromecss {
    local PROFILE_DIR="$(echo $(cd ${HOME}/.librewolf/*.default/ && pwd))"  # https://stackoverflow.com/questions/3915040
    local DIR="${PROFILE_DIR}/chrome/"
    mkdir -p "${DIR}"
    local FILE="userChrome.css"
    local FFP="${DIR}/${FILE}"

    echo "creating ${FFP}"
    cat > "${FFP}" << 'HEREDOC'
#TabsToolbar {
  visibility: collapse !important;
}
HEREDOC
}


function setup_spectacle {
    local FILE="spectaclerc"
    local DIR="$(echo $(cd ${HOME}/.config/ && pwd))"  # https://stackoverflow.com/questions/3915040
    local FFP="${DIR}/${FILE}"

    echo "creating ${FFP}"
    cat > "${FFP}" << HEREDOC
[$Version]
update_info=spectacle_newConfig.upd:spectacle-new-config

[General]
autoSaveImage=true
rememberLastRectangularRegion=false
showMagnifier=true
useLightMaskColour=true

[GuiConfig]
captureMode=5
window-position=0,0

[Save]
defaultSaveLocation=file://${HOME}/Downloads
saveFilenameFormat=screenshot_%Y%M%D_%H%m%S-%T
HEREDOC

    # -r is just a workaround so the program is not taking a screenshot
    #TBD start program minimized
    spectacle -r &
}


function move_files_to_bookmarkmanagement {
    local TARGET_PATH="${HOME}/bookmarkmanagement/"
    local SOURCE_PATH="${HOME}/Downloads/"

    # bm-bookmarking file
    cd "${SOURCE_PATH}"
    local FILENAME="$(ls -1 bm*)"

    if test -f "${FILENAME}"
    then
        echo mv -it "${TARGET_PATH}" "${FILENAME}"
        mv -it "${TARGET_PATH}" "${FILENAME}"
        cd "${TARGET_PATH}"
        bash "${TARGET_PATH}/${FILENAME}"
    fi

    # create-bm-file file
    cd "${SOURCE_PATH}"
    local FILENAME="create_backupfile_of_librewolf_bookmarks.bash"

    if test -f "${FILENAME}"
    then
        chmod +x "${FILENAME}"
        echo mv -it "${TARGET_PATH}" "${FILENAME}"
        mv -it "${TARGET_PATH}" "${FILENAME}"
    fi
}


establish_base
config_desktop_settings &
establish_librewolf &
setup_spectacle &
wait
move_files_to_bookmarkmanagement
install_stuff_via_apt


echo -e "\e[32m end of script reached \e[0m"
#EOF
