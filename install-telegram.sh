#!/bin/bash
echo version 2023-0218-1
HOME=~

#set -e -> moved to local level into functions
# "set -e" is bad coding style
#TBD: replace "set -e" because that is bad practice
#  https://stackoverflow.com/questions/19622198/what-does-set-e-mean-in-a-bash-script
#  https://unix.stackexchange.com/questions/510269/which-is-the-best-way-to-check-return-result
#  https://stackoverflow.com/questions/3474526/stop-on-first-error
#  https://www.oreilly.com/library/view/linux-shell-scripting/9781785881985/7d3e28d2-08bf-43d6-a55d-8125446019fe.xhtml


# Telegram download page: https://telegram.org/desktop/linux
# https://github.com/telegramdesktop/tdesktop/releases
#TELEGRAM_VERSION="4.6.1"
TELEGRAM_VERSION="autodetect"


# ----- do not edit below this line -----

# erste Aktion, damit die Passwortabfrage vom Benutzer gleich am Anfang durchgefuhrt werden kann und das Skript ohne Benutzerinteraktion durchlaufen kann
#  -> first action, so that the password request can be performed by the user right at the beginning and the script can run through without user interaction
# first action because the passwort input must be done at the beginning so the script can be running without user interaction
#  -> erste Aktion, da die Passworteingabe zu Beginn erfolgen muss, damit das Skript ohne Benutzerinteraktion ausgeführt werden kann
sudo true


function install_telegram_via_appimage {
set -e
    #sudo apt install -y telegram-desktop 1> "${FFP_LOG_APT}" 2>&1
    echo "preparing installation of Telegram"
    cd "${HOME}/tmp/"
    TARGETDIR="${HOME}/software/telegram-${TELEGRAM_VERSION}"
    ARCHIVE="tsetup.${TELEGRAM_VERSION}.tar.xz"

    echo "downloading Telegram"
    wget -O "${ARCHIVE}" https://telegram.org/dl/desktop/linux -nv |& tee telegram.wget.log
    # wget -O "${ARCHIVE}" "https://updates.tdesktop.com/tlinux/${ARCHIVE}"
    # wget -O "${ARCHIVE}" "https://github.com/telegramdesktop/tdesktop/releases/download/v${TELEGRAM_VERSION}/${ARCHIVE}"

    echo "extracting Telegram archive"
    mkdir "${TARGETDIR}"
    tar -xf "${ARCHIVE}" -C "${TARGETDIR}"
    rm "${ARCHIVE}"
    mv -it "${TARGETDIR}" telegram.wget.log

    echo "creating symlink"
    sudo rm -rf /usr/bin/telegram
    sudo ln -s "${TARGETDIR}/Telegram/Telegram" /usr/bin/telegram

>"${HOME}/.local/share/applications/telegram.desktop" \
echo '[Desktop Entry]
Name=Telegram Desktop
Keywords=telegram
Exec=telegram
Terminal=false
Type=Application
Icon=telegram
StartupNotify=true
'

    echo "Telegram established"
}



install_telegram_via_appimage


echo -e "\e[32m end of script reached \e[0m"
#EOF
